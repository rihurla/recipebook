//
//  CachedRecipeRequestManager.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
import Disk

class CachedRecipeRequestManager: NSObject, CachedRecipeRequestManaging {
  
  var cacheName: String = "recipes.json"
  var cacheDateKey: String = "cache_request_date"
  var cacheTimeInMinutes: Int = 60
  var requestManager: RecipeRequestManaging
  
  init(requestManager: RecipeRequestManaging) {
    self.requestManager = requestManager
    super.init()
  }
  
  /**
   Loads recipes from cache if avilable, otherwise forward request
   */
  func loadRecipes(successHandler: @escaping ([Recipe]) -> Void, errorHandler: @escaping (Error) -> Void) {
    
    if shouldClearCache() {
      self.clearCache()
    }
    
    let cachedRecipes = retrieveCachedRecipes()
    if !cachedRecipes.isEmpty {
      successHandler(cachedRecipes)
    } else {
      requestManager.requestRecipes(successHandler: { (recipes) in
        self.saveRecipesToCache(recipes: recipes)
        successHandler(recipes)
      }) { (error) in
        errorHandler(error)
      }
    }
  }
  
  /**
   Saves recipes to cache
   */
  func saveRecipesToCache(recipes: [Recipe]) {
    do {
      try Disk.save(recipes, to: .caches, as: cacheName)
    } catch {
      NSLog("Could not save recipes to cache. \(error.localizedDescription)")
    }
    UserDefaults.standard.set(Date(), forKey: cacheDateKey)
  }
  
  /**
   Retrive recipes from cache
   */
  func retrieveCachedRecipes() -> [Recipe] {
    
    var retrievedRecipes = [Recipe]()
    
    if Disk.exists(cacheName, in: .caches) {
      do {
        retrievedRecipes = try Disk.retrieve(cacheName, from: .caches, as: [Recipe].self)
      } catch {
        NSLog("Could not retrieve recipes from cache. \(error.localizedDescription)")
      }
    }

    return retrievedRecipes
  }
  
  /**
   Returns true if the cache is on the system for more than one hour
   */
  func shouldClearCache() -> Bool {
    
    let clearCache: Bool
    if UserDefaults.standard.object(forKey: cacheDateKey) != nil,
      let cacheDate = UserDefaults.standard.object(forKey: cacheDateKey) as? Date,
      Date() > cacheDate.adding(minutes: cacheTimeInMinutes) {
      clearCache = true
    } else {
      clearCache = false
    }
    
    return clearCache
  }
  
  /**
   Deletes the request cache
   */
  func clearCache() {
    if Disk.exists(cacheName, in: .caches) {
      do {
        try Disk.clear(.caches)
      } catch {
        NSLog("Could not retrieve recipes from cache. \(error.localizedDescription)")
      }
    }
  }
}
