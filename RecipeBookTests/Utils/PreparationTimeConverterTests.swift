//
//  PreparationTimeConverterTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class PreparationTimeConverterTests: XCTestCase {

  let unitUnderTest = PreparationTimeConverter.self
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }

  func testStringFromPreparationTimers() {
    let preparationTimer:[Int] = [2,2,3,3]
    let expectedTimerTotal = "10 minutes"
    let totalPrepTime = unitUnderTest.stringFromPreparationTimers(timers: preparationTimer)
    XCTAssertEqual(totalPrepTime, expectedTimerTotal)
  }


}
