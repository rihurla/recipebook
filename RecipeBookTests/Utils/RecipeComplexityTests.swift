//
//  RecipeComplexityTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class RecipeComplexityTests: XCTestCase {

  let unitUnderTest = RecipeComplexity.self
  var recipes = [Recipe]()
  
  override func setUp() {
    super.setUp()
    
    let ingredient = Ingredient(name: "Milk", quantity: "300ml", type: "Dairy")
    let pizza = Recipe(name: "Pizza",
                       ingredients: [ingredient, ingredient],
                       steps: ["Prep step 1"],
                       timers: [5],
                       imageURL: "",
                       originalURL: nil)
    
    let pie = Recipe(name: "Pie",
                     ingredients: [ingredient, ingredient, ingredient, ingredient],
                     steps: ["Prep step 1", "Prep step 2", "Prep step 3", "Prep step 4"],
                     timers: [15],
                     imageURL: "",
                     originalURL: nil)
    
    let cake = Recipe(name: "Cake",
                      ingredients: [ingredient, ingredient, ingredient, ingredient, ingredient, ingredient],
                      steps: ["Prep step 1", "Prep step 2", "Prep step 3", "Prep step 4", "Prep step 5", "Prep step 6"],
                      timers: [15],
                      imageURL: "",
                      originalURL: nil)
    
    recipes = [pizza, pie, cake]
    
  }
  
  override func tearDown() {
    super.tearDown()
    recipes.removeAll()
  }
  
  func testGetAverage() {
    let expectedAverageValue = 4.0
    let averageIngredients = unitUnderTest.getAverage(value: recipes.map { $0.ingredients.count })
    XCTAssertEqual(averageIngredients, expectedAverageValue)
  }
  
  func testComplexityOfEasy() {
    let expectedComplexity: Complexity = .easy
    let recipe = recipes[0]
    let averageIngredients = unitUnderTest.getAverage(value: recipes.map { $0.ingredients.count })
    let complexity = unitUnderTest.complexityOf(recipe: recipe, averageIngredients: averageIngredients, averageSteps: averageIngredients)
    XCTAssertEqual(complexity, expectedComplexity)
  }

  func testComplexityOfMedium() {
    let expectedComplexity: Complexity = .medium
    let recipe = recipes[1]
    let averageIngredients = unitUnderTest.getAverage(value: recipes.map { $0.ingredients.count })
    let complexity = unitUnderTest.complexityOf(recipe: recipe, averageIngredients: averageIngredients, averageSteps: averageIngredients)
    XCTAssertEqual(complexity, expectedComplexity)
  }
  
  func testComplexityOfHard() {
    let expectedComplexity: Complexity = .hard
    let recipe = recipes[2]
    let averageIngredients = unitUnderTest.getAverage(value: recipes.map { $0.ingredients.count })
    let complexity = unitUnderTest.complexityOf(recipe: recipe, averageIngredients: averageIngredients, averageSteps: averageIngredients)
    XCTAssertEqual(complexity, expectedComplexity)
  }

}
