//
//  CachedRecipeRequestManagerTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class CachedRecipeRequestManagerTests: XCTestCase {
  
  var mockedRecipes = [Recipe]()
  let unitUnderTest = CachedRecipeRequestManagerMock(cacheName: "test_recipes.json",
                                                     cacheDateKey: "test_recipe_date",
                                                     cacheTimeInMinutes: 1,
                                                     requestManager: RecipeRequestManagerMock())
  
  override func setUp() {
    super.setUp()
    let ingredient = Ingredient(name: "Milk", quantity: "300ml", type: "Dairy")
    let pizza = Recipe(name: "Pizza",
                       ingredients: [ingredient, ingredient, ingredient, ingredient, ingredient, ingredient],
                       steps: ["Prep step 1", "Prep step 2", "Prep step 3", "Prep step 4", "Prep step 5"],
                       timers: [5],
                       imageURL: "",
                       originalURL: nil)
    
    let pie = Recipe(name: "Pie",
                     ingredients: [ingredient],
                     steps: ["Prep step"],
                     timers: [15],
                     imageURL: "",
                     originalURL: nil)
    
    let cake = Recipe(name: "Cake",
                      ingredients: [ingredient],
                      steps: ["Prep step"],
                      timers: [25],
                      imageURL: "",
                      originalURL: nil)
    
    mockedRecipes = [pizza, pie, cake]
  }
  
  override func tearDown() {
    super.tearDown()
    mockedRecipes.removeAll()
    unitUnderTest.clearCache()
  }
  
  func testRequestRecipes() {
    let requestExpectation = self.expectation(description: "Request")
    unitUnderTest.loadRecipes(successHandler: { (recipes) in
      XCTAssertEqual(recipes.count, 2)
      requestExpectation.fulfill()
    }) { (error) in
      XCTFail(error.localizedDescription)
    }

    self.wait(for: [requestExpectation], timeout: 5.0)
  }
  
  func testSaveAndRetrieveRecipesToCache() {
    unitUnderTest.saveRecipesToCache(recipes: mockedRecipes)
    let recipes = unitUnderTest.retrieveCachedRecipes()
    XCTAssertEqual(recipes.count, mockedRecipes.count)
  }
  
  func testShouldClearCache() {
    unitUnderTest.saveRecipesToCache(recipes: mockedRecipes)
    XCTAssertEqual(unitUnderTest.shouldClearCache(), true)
  }
  
  func testClearCache() {
    unitUnderTest.saveRecipesToCache(recipes: mockedRecipes)
    unitUnderTest.clearCache()
    XCTAssertEqual(unitUnderTest.shouldClearCache(), false)
  }
  
}
