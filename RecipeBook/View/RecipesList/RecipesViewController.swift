//
//  ViewController.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class RecipesViewController: UIViewController {

  let projectCellHeight: CGFloat = 200.0
  let insetSpace: CGFloat = 10.0
  var bottomSpace: CGFloat = 10.0
  let interSpace: CGFloat = 15.0
  let appRedColor = UIColor(red: 0.969, green: 0.347, blue: 0.345, alpha: 1.0)
  let viewModel = RecipesViewModel()
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    viewModel.delegate = self
    viewModel.requestRecipeList()
    configureViewController()
    configureNavBar()
    configureCollectionView()
  }
  
  // MARK: UI Configuration
  func configureViewController() {
    self.definesPresentationContext = true
    self.title = NSLocalizedString("recipe_view_controller_title", comment: "Recipes view controller title")
  }
  
  func configureNavBar() {
    NavigationControllerConfigurationContainer().configureNavigationController(for: self)
  }

  func configureCollectionView() {
    
    if #available(iOS 11.0, *) {
      let insets = view.window?.safeAreaInsets
      if let bottom = insets?.bottom {
        bottomSpace += bottom
      }
    }
    
    let collectionViewCellNib = UINib(nibName: "RecipeCollectionViewCell", bundle: nil)
    let collectionViewHeaderNib = UINib(nibName: "HeaderCollectionReusableView", bundle: nil)
    collectionView.register(collectionViewHeaderNib,
                            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                            withReuseIdentifier: HeaderCollectionReusableView.headerReuseIdentifier)
    collectionView.register(collectionViewCellNib, forCellWithReuseIdentifier: RecipeCollectionViewCell.reuseIdentifier)
    collectionView.delegate = self
    collectionView.dataSource = self.viewModel
    collectionView.backgroundColor = UIColor.clear
    let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
    layout?.sectionHeadersPinToVisibleBounds = true
  }
  
  // MARK: UI Updates
  func refreshCollectionView() {
    if isViewLoaded {
      collectionView.reloadData()
      collectionView.setContentOffset(CGPoint.zero, animated: true)
    }
  }
  
  // MARK: Content Filter
  func presentFilterOptionsFor(type: HeaderAction) {
    navigationItem.searchController?.isActive = false
    switch type {
    case .complexity:
      actionSheetForComplexity()
    case .time:
      actionSheetForTime()
    }
  }

  func actionSheetForComplexity() {
    let alertTitle = NSLocalizedString("action_sheet_complexity_title", comment: "complexity title")
    let alertMessage = NSLocalizedString("action_sheet_complexity_subtitle", comment: "complexity subtitle")
    let alertCancelAction = NSLocalizedString("action_sheet_cancel", comment: "cancel")
    let alertAllAction = NSLocalizedString("action_sheet_all", comment: "all")
    let alertEasyAction = NSLocalizedString("action_sheet_complexity_easy", comment: "complexity easy")
    let alertMediumAction = NSLocalizedString("action_sheet_complexity_medium", comment: "complexity medium")
    let alertHardAction = NSLocalizedString("action_sheet_complexity_hard", comment: "complexity hard")
    
    let actionSheetController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .actionSheet)
    actionSheetController.view.tintColor = appRedColor
    
    let cancelActionButton = UIAlertAction(title: alertCancelAction, style: .cancel) { action -> Void in }
    
    let allActionButton = UIAlertAction(title: alertAllAction,style: .default) { action -> Void in
      self.viewModel.resetList()
    }
    
    let easyActionButton = UIAlertAction(title: alertEasyAction, style: .default) { action -> Void in
      self.viewModel.filterBy(complexity: .easy)
    }
    
    let mediumActionButton = UIAlertAction(title: alertMediumAction, style: .default) { action -> Void in
      self.viewModel.filterBy(complexity: .medium)
    }
    
    let hardActionButton = UIAlertAction(title: alertHardAction, style: .default) { action -> Void in
      self.viewModel.filterBy(complexity: .hard)
    }
    
    actionSheetController.addAction(cancelActionButton)
    actionSheetController.addAction(allActionButton)
    actionSheetController.addAction(easyActionButton)
    actionSheetController.addAction(mediumActionButton)
    actionSheetController.addAction(hardActionButton)
    self.present(actionSheetController, animated: true, completion: nil)
  }
  
  func actionSheetForTime() {
    
    let alertTitle = NSLocalizedString("action_sheet_preparation_title", comment: "preparation title")
    let alertMessage = NSLocalizedString("action_sheet_preparation_subtitle", comment: "preparation subtitle")
    let alertCancelAction = NSLocalizedString("action_sheet_cancel", comment: "cancel")
    let alertAllAction = NSLocalizedString("action_sheet_all", comment: "all")
    let alertShortAction = NSLocalizedString("action_sheet_preparation_short", comment: "preparation short")
    let alertMediumAction = NSLocalizedString("action_sheet_preparation_medium", comment: "preparation medium")
    let alertLongAction = NSLocalizedString("action_sheet_preparation_long", comment: "preparation long")
    let actionSheetController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .actionSheet)
    actionSheetController.view.tintColor = appRedColor
    
    let cancelActionButton = UIAlertAction(title: alertCancelAction, style: .cancel) { action -> Void in }
    
    let allActionButton = UIAlertAction(title: alertAllAction, style: .default) { action -> Void in
      self.viewModel.resetList()
    }
    
    let shortActionButton = UIAlertAction(title: alertShortAction, style: .default) { action -> Void in
      self.viewModel.filterBy(preparationTime: .short)
    }
    
    let mediumActionButton = UIAlertAction(title: alertMediumAction, style: .default) { action -> Void in
      self.viewModel.filterBy(preparationTime: .medium)
    }
    
    let longActionButton = UIAlertAction(title: alertLongAction, style: .default) { action -> Void in
      self.viewModel.filterBy(preparationTime: .long)
    }
    
    actionSheetController.addAction(cancelActionButton)
    actionSheetController.addAction(allActionButton)
    actionSheetController.addAction(shortActionButton)
    actionSheetController.addAction(mediumActionButton)
    actionSheetController.addAction(longActionButton)
    self.present(actionSheetController, animated: true, completion: nil)
    
  }
  
  func displayErrorWith(message: String) {
    let alert = UIAlertController(title: NSLocalizedString("alert_error_title", comment: "error title"),
                                  message: message,
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString("alert_error_option", comment: "error ok option"),
                                  style: .default, handler: { action in }))
    self.present(alert, animated: true, completion: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "toRecipeDetail") {
      if let recipeDetailVC = segue.destination as? RecipeDetailViewController {
        recipeDetailVC.viewModel.selectedRecipe = self.viewModel.selectedRecipe
      }
    }
  }
  
}
