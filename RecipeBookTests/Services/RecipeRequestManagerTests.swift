//
//  RecipeRequestManagerTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class RecipeRequestManagerTests: XCTestCase {

  let unitUnderTest: RecipeRequestManaging = RecipeRequestManagerMock()
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testRequestRecipes() {
    let requestExpectation = self.expectation(description: "Request")
    unitUnderTest.requestRecipes(successHandler: { (recipes) in
      XCTAssertEqual(recipes.count, 2)
      requestExpectation.fulfill()
    }) { (error) in
      XCTFail(error.localizedDescription)
    }
    self.wait(for: [requestExpectation], timeout: 5.0)
  }

}
