//
//  RecipesViewModel.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

protocol RecipesViewModelDelegate: class {
  func viewModelDidFinisheRecipeRequestWithSuccess(_ viewModel: RecipesViewModel)
  func viewModel(_ viewModel: RecipesViewModel, didFinisheRecipeRequestWith error: Error)
  func viewModelDidPerformFilter(_ viewModel: RecipesViewModel)
  func viewModelDidResetList(_ viewModel: RecipesViewModel)
  func viewModel(_ viewModel: RecipesViewModel, didRequestFilterOptionsFor type: HeaderAction)
}

class RecipesViewModel: NSObject {
  
  let headerHeight: CGFloat = 50.0
  weak var delegate: RecipesViewModelDelegate?
  var recipeRequestManager: RecipeRequestManaging = RecipeRequestManager()
  var cachedRequestManager: CachedRecipeRequestManaging
  var recipes = [Recipe]()
  var filteredRecipes = [Recipe]()
  var collectionViewHeaderView: HeaderCollectionReusableView?
  var selectedRecipe: Recipe?
  
  override init() {
    cachedRequestManager = CachedRecipeRequestManager(requestManager: recipeRequestManager)
    super.init()
  }
  
  // MARK: Content Request
  func requestRecipeList() {
    
    cachedRequestManager.loadRecipes(successHandler: { (recipes) in
      self.recipes = recipes
      self.filteredRecipes = recipes
      self.delegate?.viewModelDidFinisheRecipeRequestWithSuccess(self)
    }) { (error) in
      self.delegate?.viewModel(self, didFinisheRecipeRequestWith: error)
    }

  }
  
  func refresh() {
    requestRecipeList()
  }
  
  // MARK: Collection View Data Source Provider
  func numberOfSections() -> Int {
    return 1
  }
  
  func numberOfItems() -> Int {
    return filteredRecipes.count
  }
  
  func itemFor(indexPath: IndexPath) -> Recipe {
    return filteredRecipes[indexPath.item]
  }
  
  func selectItemFor(indexPath: IndexPath) {
    self.selectedRecipe = filteredRecipes[indexPath.item]
  }
  
  // MARK: Search and Filter
  func searchWith(text: String) {
    filteredRecipes = RecipeFilter.filterBySearch(recipes: recipes, text: text)
    self.delegate?.viewModelDidPerformFilter(self)
  }
  
  func filterBy(complexity: Complexity) {
    filteredRecipes = RecipeFilter.filterByComplexity(recipes: recipes, complexity: complexity)
    self.delegate?.viewModelDidPerformFilter(self)
    
    var buttonTitle: String
    switch complexity {
    case .easy:
      buttonTitle = NSLocalizedString("action_sheet_complexity_easy", comment: "complexity easy")
    case .medium:
      buttonTitle = NSLocalizedString("action_sheet_complexity_medium", comment: "complexity medium")
    case .hard:
      buttonTitle = NSLocalizedString("action_sheet_complexity_hard", comment: "complexity hard")
    }
    collectionViewHeaderView?.complexityButton.setTitle(buttonTitle, for: .normal)
  }
  
  func filterBy(preparationTime: PreparationTime) {
    filteredRecipes = RecipeFilter.filterByPreparationTime(recipes: recipes, preparationTime: preparationTime)
    self.delegate?.viewModelDidPerformFilter(self)

    var buttonTitle: String
    switch preparationTime {
    case .short:
      buttonTitle = NSLocalizedString("action_sheet_preparation_short", comment: "prep short")
    case .medium:
      buttonTitle = NSLocalizedString("action_sheet_preparation_medium", comment: "prep medium")
    case .long:
      buttonTitle = NSLocalizedString("action_sheet_preparation_long", comment: "prep long")
    }
    collectionViewHeaderView?.timeButton.setTitle(buttonTitle, for: .normal)
  }
  
  func resetList() {
    filteredRecipes = recipes
    self.delegate?.viewModelDidResetList(self)
    let complexityButtonTitle = NSLocalizedString("action_sheet_complexity_title", comment: "complexity title")
    let preparationButtonTitle = NSLocalizedString("action_sheet_preparation_button_title", comment: "preparation title")
    collectionViewHeaderView?.complexityButton.setTitle(complexityButtonTitle, for: .normal)
    collectionViewHeaderView?.timeButton.setTitle(preparationButtonTitle, for: .normal)
  }
  
}

// MARK: Collection View Data Source
extension RecipesViewModel: UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return numberOfSections()
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return numberOfItems()
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecipeCollectionViewCell.reuseIdentifier,
                                                  for: indexPath) as! RecipeCollectionViewCell
    cell.setUp(with: itemFor(indexPath: indexPath))
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      viewForSupplementaryElementOfKind kind: String,
                      at indexPath: IndexPath) -> UICollectionReusableView {
    let headerKind = UICollectionView.elementKindSectionHeader
    switch kind {
    case headerKind:
      let reuseIdentifier = HeaderCollectionReusableView.headerReuseIdentifier
      let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: headerKind,
                                                                         withReuseIdentifier: reuseIdentifier,
                                                                         for: indexPath) as! HeaderCollectionReusableView
      reusableview.frame = CGRect(x: 0, y: 0, width: collectionView.frame.width, height: headerHeight)
      reusableview.delegate = self
      collectionViewHeaderView = reusableview
      return reusableview
    default: fatalError("Unexpected element kind")
    }
  }
  
}

// MARK: Header Collection View Delegate
extension RecipesViewModel: HeaderCollectionReusableViewDelegate {
  func headerView(_ headerView: HeaderCollectionReusableView, didTap action: HeaderAction) {
    delegate?.viewModel(self, didRequestFilterOptionsFor: action)
  }
}
