//
//  RecipeFilter.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

class RecipeFilter: NSObject {
  
  /**
   Returns a filtered array of recipes based on the search text.
   The search text is valid for name, ingredients and instructions of the recipe.
   
   - parameter recipes: List of unfiltered recipes
   - parameter text: Search text
   */
  static func filterBySearch(recipes: [Recipe], text: String) -> [Recipe] {
    let filteredByName = recipes.filter {
      $0.name.lowercased().contains(text.lowercased())
    }
    
    let filteredByIngredient = recipes.filter {
      $0.ingredients.contains(where: { $0.name.lowercased().contains(text.lowercased()) })
    }
    
    let filteredByInstructions = recipes.filter {
      $0.steps.contains(where: { $0.lowercased().contains(text.lowercased()) })
    }
    
    let results = filteredByName + filteredByIngredient + filteredByInstructions
    return results.removingDuplicates()
  }
  
  /**
   Returns a filtered array of recipes based on its complexity.
   The complexity of the recipe is made by number of ingredients or number of steps compared to the average.
   Complexity classification: easy, medium, hard
   
   - parameter recipes: List of unfiltered recipes
   - parameter complexity: Selected complexity classification
   */
  static func filterByComplexity(recipes: [Recipe], complexity: Complexity) -> [Recipe] {
    
    let ingredients = RecipeComplexity.getAverage(value: recipes.map { $0.ingredients.count })
    let steps = RecipeComplexity.getAverage(value: recipes.map { $0.steps.count })
    
    let filteredRecipes = recipes.filter {
      RecipeComplexity.complexityOf(recipe: $0, averageIngredients: ingredients, averageSteps: steps) == complexity
    }

    return filteredRecipes
  }
  
  /**
   Returns a filtered array of recipes based on its preparation time.
   The preparation time of the recipe is separated by 0-10 mintues, 11-20 minutes and 21+ minutes.
   Preparation Time classification: short, medium, long
   
   - parameter recipes: List of unfiltered recipes
   - parameter preparationTime: Selected preparation time classification
   */
  static func filterByPreparationTime(recipes: [Recipe], preparationTime: PreparationTime) -> [Recipe] {
    
    let filteredRecipes = recipes.filter {
      RecipePreparationTime.preparationTimeFor(recipe: $0) == preparationTime
    }
    
    return filteredRecipes
  }
  
}
