//
//  ConstraintGenerator.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class ConstraintGenerator: NSObject {
  static func generateAttachmentConstraintsFor(subview: UIView, parentView: UIView) -> Array<NSLayoutConstraint> {
    
    let topConstraint = NSLayoutConstraint(item: subview,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: parentView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 0)
    
    let bottomConstraint = NSLayoutConstraint(item: subview,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: parentView,
                                              attribute: .bottom,
                                              multiplier: 1.0,
                                              constant: 0)
    
    let leadingConstraint = NSLayoutConstraint(item: subview,
                                               attribute: .leading,
                                               relatedBy: .equal,
                                               toItem: parentView,
                                               attribute: .leading,
                                               multiplier: 1.0,
                                               constant: 0)
    
    let trailingConstraint = NSLayoutConstraint(item: subview,
                                                attribute: .trailing,
                                                relatedBy: .equal,
                                                toItem: parentView,
                                                attribute: .trailing,
                                                multiplier: 1.0,
                                                constant: 0)
    
    return [topConstraint, bottomConstraint, leadingConstraint, trailingConstraint]
    
  }
  
  static func generateWidthConstraintFor(subview: UIView, parentView: UIView) -> NSLayoutConstraint {
    
    let widthConstraint = NSLayoutConstraint(item: subview,
                                             attribute: .width,
                                             relatedBy: .equal,
                                             toItem: parentView,
                                             attribute: .width,
                                             multiplier: 1.0,
                                             constant: 0.0)
    return widthConstraint
    
  }
  
  static func generateHeightConstraintFor(subview: UIView, parentView: UIView) -> NSLayoutConstraint {
    
    let heightConstraint = NSLayoutConstraint(item: subview,
                                              attribute: .height,
                                              relatedBy: .greaterThanOrEqual,
                                              toItem: parentView,
                                              attribute: .height,
                                              multiplier: 1.0,
                                              constant: 0.0)
    return heightConstraint
    
  }
  
}
