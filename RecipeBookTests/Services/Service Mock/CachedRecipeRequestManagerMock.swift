//
//  CachedRecipeRequestManagerMock.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
import Disk
@testable import RecipeBook

class CachedRecipeRequestManagerMock: NSObject, CachedRecipeRequestManaging {
  
  var cacheName: String
  var cacheDateKey: String
  var cacheTimeInMinutes: Int
  var requestManager: RecipeRequestManaging
  
  init(cacheName: String, cacheDateKey: String, cacheTimeInMinutes: Int, requestManager: RecipeRequestManaging) {
    self.cacheName = cacheName
    self.cacheDateKey = cacheDateKey
    self.cacheTimeInMinutes = cacheTimeInMinutes
    self.requestManager = requestManager
    super.init()
  }
  
  func loadRecipes(successHandler: @escaping ([Recipe]) -> Void, errorHandler: @escaping (Error) -> Void) {
    requestManager.requestRecipes(successHandler: { (recipes) in
      successHandler(recipes)
    }) { (error) in
      errorHandler(error)
    }
  }
  
  func saveRecipesToCache(recipes: [Recipe]) {
    try! Disk.save(recipes, to: .caches, as: cacheName)
  }
  
  func retrieveCachedRecipes() -> [Recipe] {
    return try! Disk.retrieve(cacheName, from: .caches, as: [Recipe].self)
  }
  
  func shouldClearCache() -> Bool {
    if Disk.exists(cacheName, in: .caches) {
      return true
    } else {
      return false
    }
  }
  
  func clearCache() {
    try! Disk.clear(.caches)
  }
  
}
