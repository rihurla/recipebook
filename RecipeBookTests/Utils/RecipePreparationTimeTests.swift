//
//  RecipePreparationTimeTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class RecipePreparationTimeTests: XCTestCase {
  
  let unitUnderTest = RecipePreparationTime.self
  var recipes = [Recipe]()

  override func setUp() {
    super.setUp()
    
    let ingredient = Ingredient(name: "Milk", quantity: "300ml", type: "Dairy")
    let pizza = Recipe(name: "Pizza",
                       ingredients: [ingredient],
                       steps: ["Prep step 1"],
                       timers: [5],
                       imageURL: "",
                       originalURL: nil)
    
    let pie = Recipe(name: "Pie",
                     ingredients: [ingredient],
                     steps: ["Prep step 1"],
                     timers: [15],
                     imageURL: "",
                     originalURL: nil)
    
    let cake = Recipe(name: "Cake",
                      ingredients: [ingredient],
                      steps: ["Prep step 1"],
                      timers: [25],
                      imageURL: "",
                      originalURL: nil)
    
    recipes = [pizza, pie, cake]
    
  }
  
  override func tearDown() {
    super.tearDown()
    recipes.removeAll()
  }

  func testPreparationTimeShort() {
    let expectedPrepTime: PreparationTime = .short
    let recipe = recipes[0]
    let prepTime = unitUnderTest.preparationTimeFor(recipe: recipe)
    XCTAssertEqual(prepTime, expectedPrepTime)
  }
  
  func testPreparationTimeMedium() {
    let expectedPrepTime: PreparationTime = .medium
    let recipe = recipes[1]
    let prepTime = unitUnderTest.preparationTimeFor(recipe: recipe)
    XCTAssertEqual(prepTime, expectedPrepTime)
  }
  
  func testPreparationTimeLong() {
    let expectedPrepTime: PreparationTime = .long
    let recipe = recipes[2]
    let prepTime = unitUnderTest.preparationTimeFor(recipe: recipe)
    XCTAssertEqual(prepTime, expectedPrepTime)
  }

}
