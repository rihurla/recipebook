![Icon](https://bitbucket.org/rihurla/recipebook/raw/f58cf877a292319d4ae3816d887749e71269072d/RecipeBook_readme_img/recipebook_icon.png)
# RecipeBook
App with selected recipes.

## App

### Prerequisites
Xcode and Simulator/iOS Device  
Cocoapods

### Installing

run `pod install` to include the dependencies

### Dependencies
Alamofire - https://github.com/Alamofire/Alamofire  
CodableAlamofire - https://github.com/Otbivnoe/CodableAlamofire  
Disk - https://github.com/saoudrizwan/Disk  
Kingfisher - https://github.com/onevcat/Kingfisher  

### Running Tests
To run the tests on Xcode, select the RecipeBook target and device, and click on the Test option or press ⌘+U

### Cache
* Request Result - Result is cached on `CachedRecipeRequestManager.swift`  using **Disk** lib.
* Images - **Kingfisher** lib downloads, caches and manages the images.

### Filters
* Search - Returns a recipe list based on the entered text.
* Complexity - Returns a recipe list based in one of the three complexities: **easy**, **medium** and **hard**.
* Time - Returns a recipe list based in one of the three time gaps: **short (0-10mins)**, **medium (11-20mins)** and **long (21+mins)**

### Complexity and Time filter classes
`RecipeComplexity.swift`  
`RecipeComplexityTests.swift`  
`RecipePreparationTime.swift`  
`RecipePreparationTimeTests.swift`  

### ScreenShots
![ScreenShot](https://bitbucket.org/rihurla/recipebook/raw/f58cf877a292319d4ae3816d887749e71269072d/RecipeBook_readme_img/recipebook_screenshot.png)

## Author
* **Ricardo Hurla** - [Portfolio](https://rihurla.com)  -  [BitBucket](https://bitbucket.org/rihurla/)
