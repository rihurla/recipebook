//
//  SectionHeaderView.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class SectionHeaderView: UITableViewHeaderFooterView {

  static let sectionHeaderReusableIdentifier = "SectionHeaderView"
  @IBOutlet weak var headerLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundView = UIView(frame: self.bounds)
    self.backgroundView?.backgroundColor = UIColor(red: 0.988, green: 0.890, blue: 0.890, alpha: 1.0)
  }
}
