//
//  DateExtensions.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

extension Date {
  /**
   Extends the Date functionality to add minutes to it
   */
  func adding(minutes: Int) -> Date {
    return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
  }
}
