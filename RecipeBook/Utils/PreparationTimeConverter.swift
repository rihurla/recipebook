//
//  CookingTimeConverter.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

class PreparationTimeConverter: NSObject {
  /**
   Receives the timers from the model and converts it to a hour/minute user friendly time
   See Localizable.stringsdict for information about the pluralization of the items
   
   - parameter timers: List of timers of the recipe
 */
  static func stringFromPreparationTimers(timers:[Int]) -> String {
    let prepTime = timers.reduce(0, +)
    let seconds = prepTime * 60
    let hour = seconds / 3600
    let minute = (seconds % 3600) / 60
    
    var timeString = ""
    if hour > 0 {
      let format = NSLocalizedString("pluralized_hours", comment: "pluralized hours format.")
      let pluralizedHours = String.localizedStringWithFormat(format, hour)
      timeString.append(pluralizedHours)
      if minute > 0 { timeString.append(NSLocalizedString("preparation_converter_connection",
                                                          comment: "and connection"))}
    }
    
    if minute > 0 {
      let format = NSLocalizedString("pluralized_minutes", comment: "pluralized minute format.")
      let pluralizedMinutes = String.localizedStringWithFormat(format, minute)
      timeString.append(pluralizedMinutes)
    }
    
    return timeString
  }
  
}
