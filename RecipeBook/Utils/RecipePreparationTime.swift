//
//  RecipePreparationTime.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

enum PreparationTime {
  case short
  case medium
  case long
}

class RecipePreparationTime: NSObject {
  
  /**
   Returns the preparation time classification of the recipe.
   The preparation time of the recipe is separated by 0-10 mintues, 11-20 minutes and 21+ minutes.
   Preparation Time classification: short, medium, long
   
   - parameter recipe: Recipe to be classified.
   */
  static func preparationTimeFor(recipe: Recipe) -> PreparationTime {
    
    var prepTime: PreparationTime
    let totalPrepTime = recipe.timers.reduce(0, +)
    
    if totalPrepTime <= 10 {
      prepTime = .short
    } else if totalPrepTime > 10 && totalPrepTime <= 20 {
      prepTime = .medium
    } else {
      prepTime = .long
    }
    
    return prepTime
  }
  
}
