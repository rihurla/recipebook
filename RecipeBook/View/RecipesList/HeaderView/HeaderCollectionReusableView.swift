//
//  HeaderCollectionReusableView.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

enum HeaderAction {
  case complexity
  case time
}

protocol HeaderCollectionReusableViewDelegate: class {
  func headerView(_ headerView: HeaderCollectionReusableView, didTap action: HeaderAction)
}

class HeaderCollectionReusableView: UICollectionReusableView {

  @IBOutlet weak var timeButton: UIButton!
  @IBOutlet weak var complexityButton: UIButton!
  static let headerReuseIdentifier = "HeaderView"
  weak var delegate: HeaderCollectionReusableViewDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
    
  @IBAction func didTapComplexityButton(_ sender: Any) {
    delegate?.headerView(self, didTap: .complexity)
  }
  
  @IBAction func didTapTimeButton(_ sender: Any) {
    delegate?.headerView(self, didTap: .time)
  }
  
}
