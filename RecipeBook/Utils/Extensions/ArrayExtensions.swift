//
//  ArrayExtensions.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
  /**
   Extends the array functionality to remove duplicated Codable Equatable Objects
   */
  func removingDuplicates() -> Array {
    return reduce(into: []) { result, element in
      if !result.contains(element) {
        result.append(element)
      }
    }
  }
}
