//
//  RecipesViewModelTest.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class RecipesViewModelTests: XCTestCase {
  
  let unitUnderTest = RecipesViewModel()
  let cachedRecipeManager = CachedRecipeRequestManagerMock(cacheName: "test_recipes.json",
                                                           cacheDateKey: "test_recipe_date",
                                                           cacheTimeInMinutes: 1,
                                                           requestManager: RecipeRequestManagerMock())
  
  var mockedRecipes = [Recipe]()
  
  override func setUp() {
    super.setUp()
    
    unitUnderTest.recipeRequestManager = cachedRecipeManager.requestManager
    unitUnderTest.cachedRequestManager = cachedRecipeManager
    
    let ingredient = Ingredient(name: "Milk", quantity: "300ml", type: "Dairy")
    let pizza = Recipe(name: "Pizza",
                       ingredients: [ingredient, ingredient, ingredient, ingredient, ingredient, ingredient],
                       steps: ["Prep step 1", "Prep step 2", "Prep step 3", "Prep step 4", "Prep step 5"],
                       timers: [5],
                       imageURL: "",
                       originalURL: nil)
    
    let pie = Recipe(name: "Pie",
                     ingredients: [ingredient],
                     steps: ["Prep step"],
                     timers: [15],
                     imageURL: "",
                     originalURL: nil)
    
    let cake = Recipe(name: "Cake",
                      ingredients: [ingredient],
                      steps: ["Prep step"],
                      timers: [25],
                      imageURL: "",
                      originalURL: nil)
    
    mockedRecipes = [pizza, pie, cake]
    
  }
  
  override func tearDown() {
    super.tearDown()
    mockedRecipes.removeAll()
    unitUnderTest.cachedRequestManager.clearCache()
  }
  
  func testRequestRecipeList() {
    let expectedSectionsCount = 2
    unitUnderTest.requestRecipeList()
    let recipesCount = unitUnderTest.recipes.count
    XCTAssertEqual(recipesCount, expectedSectionsCount)
  }
  
  func testRefresh() {
    let expectedListCount = 2
    unitUnderTest.requestRecipeList()
    unitUnderTest.recipes.append(mockedRecipes[0])
    unitUnderTest.refresh()
    let recipesCount = unitUnderTest.recipes.count
    XCTAssertEqual(recipesCount, expectedListCount)

  }

  func testNumberOfSections() {
    let expectedSectionsCount = 1
    let numberOfSections = unitUnderTest.numberOfSections()
    XCTAssertEqual(numberOfSections, expectedSectionsCount)
  }
  
  func testNumberOfItems() {
    let expectedItemsCount = 3
    unitUnderTest.recipes = mockedRecipes
    unitUnderTest.filteredRecipes = mockedRecipes
    let numberOfSections = unitUnderTest.numberOfItems()
    XCTAssertEqual(numberOfSections, expectedItemsCount)
  }
  
  func testItemFor() {
    let expectedItem = mockedRecipes[0]
    unitUnderTest.recipes = mockedRecipes
    unitUnderTest.filteredRecipes = mockedRecipes
    let item = unitUnderTest.itemFor(indexPath: IndexPath(item: 0, section: 0))
    XCTAssertEqual(item, expectedItem)
  }
  
  func testSearchWith() {
    let expectedSearchCount = 1
    unitUnderTest.recipes = mockedRecipes
    unitUnderTest.filteredRecipes = mockedRecipes
    unitUnderTest.searchWith(text: "Pizza")
    XCTAssertEqual(unitUnderTest.filteredRecipes.count, expectedSearchCount)
  }
  
  func testfilterByComplexity() {
    let expectedFilterCount = 1
    unitUnderTest.recipes = mockedRecipes
    unitUnderTest.filteredRecipes = mockedRecipes
    unitUnderTest.filterBy(complexity: .hard)
    XCTAssertEqual(unitUnderTest.filteredRecipes.count, expectedFilterCount)
  }
  
  func testfilterByPreparationTime() {
    let expectedFilterCount = 1
    unitUnderTest.recipes = mockedRecipes
    unitUnderTest.filteredRecipes = mockedRecipes
    unitUnderTest.filterBy(preparationTime: .short)
    XCTAssertEqual(unitUnderTest.filteredRecipes.count, expectedFilterCount)
  }
  
  func testResetList() {
    let expectedCount = 3
    unitUnderTest.recipes = mockedRecipes
    unitUnderTest.filteredRecipes = mockedRecipes
    unitUnderTest.filterBy(preparationTime: .short)
    unitUnderTest.resetList()
    XCTAssertEqual(unitUnderTest.filteredRecipes.count, expectedCount)
  }
  
}
