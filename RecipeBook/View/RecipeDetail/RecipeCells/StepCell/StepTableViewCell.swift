//
//  StepTableViewCell.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class StepTableViewCell: UITableViewCell {

  static let stepReusableIdentifier = "StepCell"
  @IBOutlet weak var stepLabel: UILabel!
  @IBOutlet weak var stepNumberLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  func setUp(step: String, index: Int) {
    let fixedIndex = index + 1
    let stringFormat = NSLocalizedString("step_cell_step", comment: "step title")
    let formattedStep = String.localizedStringWithFormat(stringFormat, fixedIndex)
    self.stepNumberLabel.text = formattedStep
    self.stepLabel.text = step
  }
}
