//
//  HeaderTableViewCell.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit
import Kingfisher

class HeaderTableViewCell: UITableViewCell {

  static let headerReusableIdentifier = "HeaderCell"
  @IBOutlet weak var recipeTitleLabel: UILabel!
  @IBOutlet weak var recipeImageView: UIImageView!
  @IBOutlet weak var preparationTimeLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  func setUp(title: String, imageUrl: String, timers: [Int]) {
    self.recipeTitleLabel.text = title
    let prepTime = PreparationTimeConverter.stringFromPreparationTimers(timers: timers)
    let stringFormat = NSLocalizedString("header_cell_preparation_time", comment: "prep time format")
    let formattedPrepTime = String.localizedStringWithFormat(stringFormat, prepTime)
    self.preparationTimeLabel.text = formattedPrepTime
    let image = UIImage(named: "placeholder")
    if let url = URL(string: imageUrl) {
      recipeImageView.kf.setImage(with: url, placeholder: image)
    }
  }
    
}

