//
//  RecipesViewControllerExtensions.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

// MARK: View Model Delegate
extension RecipesViewController: RecipesViewModelDelegate {
  
  func viewModel(_ viewModel: RecipesViewModel, didRequestFilterOptionsFor type: HeaderAction) {
    presentFilterOptionsFor(type: type)
  }
  
  func viewModelDidResetList(_ viewModel: RecipesViewModel) {
    refreshCollectionView()
  }
  
  func viewModelDidPerformFilter(_ viewModel: RecipesViewModel) {
    refreshCollectionView()
  }
  
  func viewModelDidFinisheRecipeRequestWithSuccess(_ viewModel: RecipesViewModel) {
    refreshCollectionView()
  }
  
  func viewModel(_ viewModel: RecipesViewModel, didFinisheRecipeRequestWith error: Error) {
    displayErrorWith(message: error.localizedDescription)
  }
}

// MARK: Search Controller Delegate
extension RecipesViewController: UISearchControllerDelegate {
  func willPresentSearchController(_ searchController: UISearchController) {
    self.viewModel.resetList()
  }
}

// MARK: Search Bar Delegate
extension RecipesViewController: UISearchBarDelegate {
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    viewModel.searchWith(text: searchText)
  }
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    viewModel.resetList()
  }
  
}

// MARK: Collection View Delegate and Flow Layout
extension RecipesViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.viewModel.selectItemFor(indexPath: indexPath)
    self.performSegue(withIdentifier: "toRecipeDetail", sender: self)
  }
  
}

extension RecipesViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: (collectionView.frame.width / 2) - interSpace, height: projectCellHeight)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: insetSpace, left: insetSpace, bottom: insetSpace, right: insetSpace)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return insetSpace
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0.0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    return CGSize(width: collectionView.frame.width, height: viewModel.headerHeight)
  }
  
}

