//
//  IngredientTableViewCell.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class IngredientTableViewCell: UITableViewCell {

  static let ingredientReusableIdentifier = "IngredientCell"
  
  @IBOutlet weak var quantityLabel: UILabel!
  @IBOutlet weak var ingredientNameLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func setUp(ingredient: Ingredient) {
    let stringFormat = NSLocalizedString("ingredient_cell_quantity", comment: "quantity format")
    let formattedQuantity = String.localizedStringWithFormat(stringFormat, ingredient.quantity)
    self.quantityLabel.text = formattedQuantity.trimmingCharacters(in: .whitespacesAndNewlines)
    let trimmedName = ingredient.name.trimmingCharacters(in: .whitespacesAndNewlines)
    self.ingredientNameLabel.text = trimmedName.capitalizingFirstLetter()
  }
    
}

