//
//  RecipeCollectionViewCell.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit
import Kingfisher

class RecipeCollectionViewCell: UICollectionViewCell {

  static let reuseIdentifier = "RecipeCell"
  @IBOutlet weak var thumbnailImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var ingredientsLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func setUp(with recipe: Recipe) {
    self.titleLabel.text = recipe.name.trimmingCharacters(in: .whitespacesAndNewlines)
    let format = NSLocalizedString("pluralized_ingredients", comment: "pluralized ingredients format.")
    let pluralizedIngredients = String.localizedStringWithFormat(format, recipe.ingredients.count)
    self.ingredientsLabel.text = pluralizedIngredients
    self.timeLabel.text = PreparationTimeConverter.stringFromPreparationTimers(timers: recipe.timers)
    
    if let imageUrl = URL(string: recipe.imageURL) {
      setUpImageWith(url: imageUrl)
    }
  }
  
  func setUpImageWith(url: URL) {
    let image = UIImage(named: "placeholder")
    thumbnailImageView.kf.setImage(with: url, placeholder: image)
  }

}

