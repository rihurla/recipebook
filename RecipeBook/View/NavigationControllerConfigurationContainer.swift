//
//  NavigationControllerConfigurationContainer.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class NavigationControllerConfigurationContainer: NSObject {
  /**
   Configures the Navigation View Controller
   */
  func configureNavigationController(for viewController: UIViewController) {
    
    configureNavBar(viewController: viewController)
    
    if let recipeViewController = viewController as? RecipesViewController {
      configureSearchController(viewController: recipeViewController)
      configureNavBarLeftItem(viewController: recipeViewController)
    }
    
  }
  
  //MARK: General Configuration
  private func configureNavBar(viewController: UIViewController) {
    viewController.navigationController?.view.backgroundColor = UIColor.white
    viewController.navigationController?.navigationBar.prefersLargeTitles = false
    viewController.navigationController?.navigationBar.tintColor = UIColor.white
    viewController.navigationController?.navigationBar.barStyle = UIBarStyle.black
  }
  
  //MARK: Recipe View Controller Especific Configuration
  private func configureSearchController(viewController: RecipesViewController) {
    let searchController = UISearchController(searchResultsController: nil)
    searchController.delegate = viewController
    searchController.searchBar.delegate = viewController
    searchController.searchBar.tintColor = UIColor.white
    searchController.dimsBackgroundDuringPresentation = false
    viewController.navigationItem.searchController = searchController
    viewController.navigationItem.hidesSearchBarWhenScrolling = false
    viewController.navigationItem.largeTitleDisplayMode = .never
  }
  
  private func configureNavBarLeftItem(viewController: RecipesViewController) {
    let image = UIImage(named: "recipebook_icon_nav")
    let imageView = UIImageView(image: image)
    imageView.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
    let widthConstraint = imageView.widthAnchor.constraint(equalToConstant: 36)
    let heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 36)
    heightConstraint.isActive = true
    widthConstraint.isActive = true
    let barButton = UIBarButtonItem(customView: imageView)
    viewController.navigationItem.leftBarButtonItem = barButton
  }
  
}
