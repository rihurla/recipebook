//
//  RecipeRequestManagerMock.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
@testable import RecipeBook

class RecipeRequestManagerMock: NSObject, RecipeRequestManaging {
  
  func requestRecipes(successHandler: @escaping ([Recipe]) -> Void, errorHandler: @escaping (Error) -> Void) {
    
    let bundle = Bundle(for: type(of: self))
    guard let filePath = bundle.path(forResource: "recipes_mock", ofType: "json") else {
      errorHandler(RecipeBookError("File not found"))
      return
    }
    
    do {
      let jsonData = try Data.init(contentsOf: URL(fileURLWithPath: filePath))
      let recipesObject = try JSONDecoder().decode([Recipe].self, from: jsonData)
      successHandler(recipesObject)
    } catch {
      errorHandler(RecipeBookError(error.localizedDescription))
    }
    
  }
  
}
