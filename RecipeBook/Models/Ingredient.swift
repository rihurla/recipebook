//
//  Ingredient.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct Ingredient: Codable {
  let name: String
  let quantity: String
  let type: String
}
