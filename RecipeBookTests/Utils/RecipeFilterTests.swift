//
//  RecipeFilterTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class RecipeFilterTests: XCTestCase {

  let unitUnderTest = RecipeFilter.self
  var recipes = [Recipe]()
  
  override func setUp() {
    super.setUp()
    
    let ingredient = Ingredient(name: "Milk", quantity: "300ml", type: "Dairy")
    let pizza = Recipe(name: "Pizza",
                       ingredients: [ingredient],
                       steps: ["Prep step"],
                       timers: [5],
                       imageURL: "",
                       originalURL: nil)
    
    let pie = Recipe(name: "Pie",
                     ingredients: [ingredient],
                     steps: ["Prep step"],
                     timers: [15],
                     imageURL: "",
                     originalURL: nil)
    
    let cake = Recipe(name: "Cake",
                      ingredients: [ingredient],
                      steps: ["Prep step"],
                      timers: [25],
                      imageURL: "",
                      originalURL: nil)
    
    recipes = [pizza, pie, cake]
    
  }
  
  override func tearDown() {
    super.tearDown()
    recipes.removeAll()
  }

  func testFilterBySearch() {
    let expectedArrayCount = 1
    let searchedItems = unitUnderTest.filterBySearch(recipes: recipes, text: "Pizza")
    XCTAssertEqual(searchedItems.count, expectedArrayCount)
  }
  
  func testFilterByPreparationTimeShort() {
    let expectedArrayCount = 1
    let shortItems = unitUnderTest.filterByPreparationTime(recipes: recipes, preparationTime: .short)
    XCTAssertEqual(shortItems.count, expectedArrayCount)
  }
  
  func testFilterByPreparationTimeMedium() {
    let expectedArrayCount = 1
    let mediumItems = unitUnderTest.filterByPreparationTime(recipes: recipes, preparationTime: .medium)
    XCTAssertEqual(mediumItems.count, expectedArrayCount)
  }
  
  func testFilterByPreparationTimeLong() {
    let expectedArrayCount = 1
    let longItems = unitUnderTest.filterByPreparationTime(recipes: recipes, preparationTime: .long)
    XCTAssertEqual(longItems.count, expectedArrayCount)
  }

}
