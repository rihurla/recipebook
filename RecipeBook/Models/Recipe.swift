//
//  Recipe.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct Recipe: Codable, Equatable {
  let name: String
  let ingredients: [Ingredient]
  let steps: [String]
  let timers: [Int]
  let imageURL: String
  let originalURL: String?
}

extension Recipe {
  static func ==(lhs: Recipe, rhs: Recipe) -> Bool {
    return lhs.name == rhs.name
  }
}
