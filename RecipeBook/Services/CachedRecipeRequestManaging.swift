//
//  CachedRecipeRequestManaging.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
/*
 Protocol used for mock creation
 */
protocol CachedRecipeRequestManaging {
  
  var cacheName: String { get set }
  var cacheDateKey: String { get set }
  var cacheTimeInMinutes: Int { get set }
  var requestManager: RecipeRequestManaging { get set }
  
  func loadRecipes(successHandler: @escaping ([Recipe]) -> Void, errorHandler: @escaping (Error) -> Void)
  func saveRecipesToCache(recipes: [Recipe])
  func retrieveCachedRecipes() -> [Recipe]
  func shouldClearCache() -> Bool
  func clearCache()
  
}
