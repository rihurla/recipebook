//
//  StringExtensions.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

extension String {
  func capitalizingFirstLetter() -> String {
    return prefix(1).uppercased() + self.lowercased().dropFirst()
  }
  
  mutating func capitalizeFirstLetter() {
    self = self.capitalizingFirstLetter()
  }
}
