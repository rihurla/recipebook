//
//  ArrayExtensionsTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class ArrayExtensionsTests: XCTestCase {

  var recipes = [Recipe]()
  
  override func setUp() {
    super.setUp()
    
    let ingredient = Ingredient(name: "Milk", quantity: "300ml", type: "Dairy")
    let pizza = Recipe(name: "Mic's Yorkshire Puds",
                       ingredients: [ingredient],
                       steps: ["Prep step"],
                       timers: [10],
                       imageURL: "",
                       originalURL: nil)
    recipes = [pizza, pizza]
    
  }
  
  override func tearDown() {
    super.tearDown()
    recipes.removeAll()
  }

  func testRemovingDuplicates() {
    let expectedArrayCount = 1
    let sortedArray = recipes.removingDuplicates()
    XCTAssertEqual(sortedArray.count, expectedArrayCount)
  }

}
