//
//  RecipeDetailViewModel.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class RecipeDetailViewModel: NSObject {
  var selectedRecipe: Recipe?
  let sectionHeaderHeight: CGFloat = 50.0
  let numberOfsections: Int = 3
}

extension RecipeDetailViewModel: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard let recipe = selectedRecipe else { return nil }
    var sectionTitle: String
    if section == 0 {
      sectionTitle = ""
    } else if section == 1 {
      let stringFormat = NSLocalizedString("recipe_detail_ingredients_title", comment: "ingredients title")
      let formattedTitle = String.localizedStringWithFormat(stringFormat, recipe.ingredients.count)
      sectionTitle = formattedTitle
    } else {
      sectionTitle = NSLocalizedString("recipe_detail_instruction_title", comment: "instructions title")
    }
    
    let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SectionHeaderView.sectionHeaderReusableIdentifier) as! SectionHeaderView
    headerView.headerLabel.text = sectionTitle
    return headerView
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 {
      return 0
    } else {
      return sectionHeaderHeight
    }
  }
}

extension RecipeDetailViewModel: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let recipe = selectedRecipe else { return UITableViewCell() }
    let currentCell: UITableViewCell
    
    if indexPath.section == 0 { // Header
      let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.headerReusableIdentifier) as! HeaderTableViewCell
      cell.setUp(title: recipe.name, imageUrl: recipe.imageURL, timers: recipe.timers)
      currentCell = cell
    } else if indexPath.section == 1 { // Ingredients
      let cell = tableView.dequeueReusableCell(withIdentifier: IngredientTableViewCell.ingredientReusableIdentifier) as! IngredientTableViewCell
      cell.setUp(ingredient: recipe.ingredients[indexPath.item])
      currentCell = cell
    } else { // Steps
      let cell = tableView.dequeueReusableCell(withIdentifier: StepTableViewCell.stepReusableIdentifier) as! StepTableViewCell
      cell.setUp(step: recipe.steps[indexPath.item], index: indexPath.item)
      currentCell = cell
    }
    
    currentCell.selectionStyle = .none
    return currentCell
    
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    guard let _ = selectedRecipe else { return 0 }
    return numberOfsections
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let recipe = selectedRecipe else { return 0 }
    let numberOfRows: Int
    if section == 0 { // Header
      numberOfRows = 1
    } else if section == 1 { // Ingredients
      numberOfRows = recipe.ingredients.count
    } else { // Steps
      numberOfRows = recipe.steps.count
    }
    return numberOfRows
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
}
