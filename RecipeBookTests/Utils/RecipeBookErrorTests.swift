//
//  RecipeBookErrorTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class RecipeBookErrorTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }

  func testRecipeBookErrorMessage() {
    let expectedMessage = "RecipeBook Error Message Test"
    let unitUnderTest = RecipeBookError(expectedMessage)
    XCTAssertEqual(unitUnderTest.localizedDescription, expectedMessage)
  }

}
