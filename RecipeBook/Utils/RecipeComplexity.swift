//
//  RecipeComplexity.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 25/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

enum Complexity {
  case easy
  case medium
  case hard
}

class RecipeComplexity: NSObject {
  
  /**
   Returns the complexity of the recipe.
   The complexity of the recipe is made by number of ingredients or number of steps compared to the average.
   Complexity classification: easy, medium, hard
   
   - parameter recipe: Recipe to be classified
   - parameter averageIngredients: Average number of ingredients
   - parameter averageIngredients: Average number of preparation steps
   */
  static func complexityOf(recipe: Recipe, averageIngredients: Double, averageSteps: Double) -> Complexity {
    
    let roundedIngredients = Int((averageIngredients/3).rounded(.up))
    let roundedSteps = Int((averageSteps/3).rounded(.up))
    var complexity: Complexity
    if recipe.ingredients.count <= roundedIngredients || recipe.steps.count <= roundedSteps {
      complexity = .easy
    } else if recipe.ingredients.count <= (roundedIngredients*2) || recipe.steps.count <= (roundedSteps*2) {
      complexity = .medium
    } else {
      complexity = .hard
    }
    
    return complexity
    
  }
  
  /**
   Returns the average number, given an array of integers
   
   - parameter value: array of integers
   */
  static func getAverage(value:[Int]) -> Double {
    return Double(value.reduce(0,+))/Double(value.count)
  }
  
}
