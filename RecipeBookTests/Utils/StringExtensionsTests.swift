//
//  StringExtensionsTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class StringExtensionsTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testCapitalizeFirstLetter() {
    let expectedString = "Recipe number 1"
    let string = "recipe number 1".capitalizingFirstLetter()
    XCTAssertEqual(string, expectedString)
  }
  
}
