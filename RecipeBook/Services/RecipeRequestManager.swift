//
//  NetworkRequestManager.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class RecipeRequestManager: NSObject, RecipeRequestManaging {
  
  /**
   Request recipes from the API.
   */
  func requestRecipes(successHandler: @escaping ([Recipe]) -> Void, errorHandler: @escaping (Error) -> Void) {
    
    let url = URL(string: ServiceUrls.recipesURL)!
    let decoder = JSONDecoder()
    
    Alamofire.request(url).responseDecodableObject(keyPath: nil,
                                                   decoder: decoder) { (response: DataResponse<[Recipe]>) in
      switch response.result {
      case .success:
        if let recipesArray = response.result.value {
          successHandler(recipesArray)
        } else {
          errorHandler(RecipeBookError("No available recipes"))
        }
      case .failure(let error):
        errorHandler(error)
      }
    }

  }
  
}
