//
//  DateExtensionsTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 27/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class DateExtensionsTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testAddingMitutesToDate() {
    let originalDate = Date()
    let expectedDate = originalDate.addingTimeInterval(5.0 * 60.0)
    let date = originalDate.adding(minutes: 5)
    XCTAssertEqual(date, expectedDate)
  }
  
}
