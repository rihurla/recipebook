//
//  NetworkRequestManaging.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
/*
 Protocol used for mock creation
 */
protocol RecipeRequestManaging {
  func requestRecipes(successHandler: @escaping ([Recipe]) -> Void, errorHandler: @escaping (Error) -> Void)
}
