//
//  ModelTests.swift
//  RecipeBookTests
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import RecipeBook

class ModelTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }

  func testRecipeModel() {
    
    let expectedRecipeName = "Crock Pot Roast"
    let expectedIngredientsCount = 5
    let bundle = Bundle(for: type(of: self))
    guard let filePath = bundle.path(forResource: "recipe_mock", ofType: "json") else {
      XCTFail("File path for recipe_mock.json not found")
      return
    }
    
    do {
      let jsonData = try Data.init(contentsOf: URL(fileURLWithPath: filePath))
      let recipeObject = try JSONDecoder().decode(Recipe.self, from: jsonData)
      XCTAssertEqual(recipeObject.name, expectedRecipeName)
      XCTAssertEqual(recipeObject.ingredients.count, expectedIngredientsCount)
    } catch {
      XCTFail("Failed to extract and decode json")
    }

  }

  func testIngredientModel() {
    
    let expectedIngredientsCount = 5
    let expectedIngredientName = " beef roast"
    let bundle = Bundle(for: type(of: self))
    guard let filePath = bundle.path(forResource: "recipe_mock", ofType: "json") else {
      XCTFail("File path for recipe_mock.json not found")
      return
    }
    
    do {
      let jsonData = try Data.init(contentsOf: URL(fileURLWithPath: filePath))
      let recipeObject = try JSONDecoder().decode(Recipe.self, from: jsonData)
      XCTAssertEqual(recipeObject.ingredients.count, expectedIngredientsCount)
      XCTAssertEqual(recipeObject.ingredients[0].name, expectedIngredientName)
    } catch {
      XCTFail("Failed to extract and decode json")
    }
  }
  
}
