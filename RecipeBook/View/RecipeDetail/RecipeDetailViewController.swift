//
//  RecipeDetailViewController.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 26/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class RecipeDetailViewController: UIViewController {

  let viewModel = RecipeDetailViewModel()
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
    configureTableView()
  }
  
  func configureUI() {
    self.navigationItem.largeTitleDisplayMode = .never
    self.title = NSLocalizedString("recipe_detail_view_controller_title", comment: "details title")
  }
  
  func configureTableView() {
    tableView.delegate = viewModel
    tableView.dataSource = viewModel
    tableView.estimatedRowHeight = 55
    tableView.rowHeight = UITableView.automaticDimension
    tableView.tableFooterView = UIView.init()
    let sectionHeaderNib = UINib.init(nibName: "SectionHeaderView", bundle: Bundle.main)
    let headerNib = UINib.init(nibName: "HeaderTableViewCell", bundle: nil)
    let ingredientsNib = UINib.init(nibName: "IngredientTableViewCell", bundle: nil)
    let stepsNib = UINib.init(nibName: "StepTableViewCell", bundle: nil)
    tableView.register(sectionHeaderNib, forHeaderFooterViewReuseIdentifier: SectionHeaderView.sectionHeaderReusableIdentifier)
    tableView.register(headerNib, forCellReuseIdentifier: HeaderTableViewCell.headerReusableIdentifier)
    tableView.register(ingredientsNib, forCellReuseIdentifier: IngredientTableViewCell.ingredientReusableIdentifier)
    tableView.register(stepsNib, forCellReuseIdentifier: StepTableViewCell.stepReusableIdentifier)
  }
  
}
