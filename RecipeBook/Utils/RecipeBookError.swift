//
//  RecipeBookError.swift
//  RecipeBook
//
//  Created by Ricardo Hurla on 24/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

/*
 Custom Localized error struct
 */
struct RecipeBookError: Error {
  
  let message: String
  
  init(_ message: String) {
    self.message = message
  }
  
  public var localizedDescription: String {
    return message
  }
  
}
